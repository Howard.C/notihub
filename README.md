# NotiHub 
## Senario
Build a NotiHub to analyzing alarm and maintain customer environment with automated scripts 

<p align="center">
    <img src="./images/notihub_ architecture.png" width="100%" height="100%">
</p>
<p align="center">
    <img src="./notihub_Flow.png" width="100%" height="100%">
</p>

### Create a web server hosting by EC2
Create a EC2 instance and put your web on it.
### CloudWatch  
Create alarm → Select matrics → Configure action (HealthyHostCount) → SNS
## Create EventBridge for message delivery

### EventBus

Use Event Bus can send events to other AWS accounts, or receive messages from other AWS accounts.

- Sender : (Account ID: 863936362823)
    - Set up __EventRule__ that trigger the receiver account's default event bus as the target.
<p align="center">
    <img src="./images/event_pattern.png" width="100%" height="100%">
</p>

```JSON
{
  "source": ["aws.cloudwatch"],
  "detail-type": ["CloudWatch Alarm State Change"],
  "resources": [
    "arn:aws:cloudwatch:us-east-2:<Account-id>:alarm:<name-of-alarm>"
  ]
}
```

- Select the another account eventbus as Target and **make sure the role have the right policy!!**.
<p align="center">
    <img src="./images/select_target.png" width="100%" height="100%">
</p>

- Replace your region.
<p align="center">
    <img src="./images/event_invoke.png" width="100%" height="100%">
</p>

- Receiver : (Account ID: 193028834773)
    1. Edit the permissions on the default Eventbus to allow specified AWS accounts.
    2. Set up rules that match events that come from the sender account.
```JSON
{
  "Version": "2012-10-17",
  "Statement": [{
    "Sid": "allow_account_to_put_events",
    "Effect": "Allow",
    "Principal": {
      "AWS": "arn:aws:iam::<Sender-ACCOUNT_ID>:root"
    },
    "Action": "events:PutEvents",
    "Resource": "arn:aws:events:us-west-1:<MY-ACCOUNT_ID>:event-bus/default"
  }, {
    "Sid": "allow_account_to_manage_rules_they_created",
    "Effect": "Allow",
    "Principal": {
      "AWS": "arn:aws:iam::<Sender-ACCOUNT_ID>:root"
    },
    "Action": ["events:PutRule", "events:PutTargets", "events:DeleteRule", "events:RemoveTargets", "events:DisableRule", "events:EnableRule", "events:TagResource", "events:UntagResource", "events:DescribeRule", "events:ListTargetsByRule", "events:ListTagsForResource"],
    "Resource": "arn:aws:events:us-west-1:<MY-ACCOUNT_ID>:rule/default",
    "Condition": {
      "StringEqualsIfExists": {
        "events:creatorAccount": "<Sender-ACCOUNT_ID>"
      }
    }
  }]
}
```
<p align="center">
    <img src="./images/receiver_pattern.png" width="100%" height="100%">
</p>
<p align="center">
    <img src="./images/receiver_target.png" width="100%" height="100%">
</p>

### Switch Role & Assume Role & MFA
1. Create role
    - Creating a role to delegate permissions to an IAM user

2. Switch Role using boto3

```Python 
import boto3
import pyotp

# get MFA six number
totp = pyotp.TOTP('<MFA token>') #paste your mfa token
mfa_code = totp.now()

# with this account aws_access_key_id & aws_secret_access_key
sts_client = boto3.client('sts')

assumed_role_object=sts_client.assume_role(
    RoleArn="arn:aws:iam::<account-id>:role/<name-of-role>", # another account role
    RoleSessionName="AssumeRoleSession1",
    SerialNumber="arn:aws:iam::<account-id>:mfa/<user-name>", # this account mfa role
    TokenCode=mfa_code
)

credentials=assumed_role_object['Credentials']

s3_resource=boto3.resource(
    's3',
    aws_access_key_id=credentials['AccessKeyId'],
    aws_secret_access_key=credentials['SecretAccessKey'],
    aws_session_token=credentials['SessionToken'],
)

for bucket in s3_resource.buckets.all():
    print(bucket.name)
# when you can list your S3 bucket name, it means you did tje right way.

```
```JSON
# ERROR handling
botocore.exceptions.ClientError: An error occurred (AccessDenied) when calling the AssumeRole operation: User: arn:aws:iam::193028834773:user/season.wang is not authorized to perform: sts:AssumeRole on resource: arn:aws:iam::863936362823:role/season-account-role

1. check the mfa token and code
2. add sts permission
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "sts:*"
            ],
            "Resource": "*"
        }
    ]
}

```

